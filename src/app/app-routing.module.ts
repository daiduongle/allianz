import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthLayoutComponent } from './shared/components/auth-layout/auth-layout.component';
import { AccountErrorComponent } from './shared/components/account-error/account-error.component';

const routes: Routes = [
    {
        path: '',
        component: AuthLayoutComponent,
        children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            {
                path: 'login',
                loadChildren: () =>
                    import('./shared/components/login/login.module').then(
                        mod => mod.LoginModule
                    )
            },
            {
                path: 'register',
                loadChildren: () =>
                    import('./shared/components/register/register.module').then(
                        mod => mod.RegisterModule
                    )
            },
            {
                path: 'forgot-password',
                loadChildren: () =>
                    import('./shared/components/forgot-password/forgot-password.module').then(
                        mod => mod.ForgotPasswordModule
                    )
            },
            {
                path: 'forgot-id',
                loadChildren: () =>
                    import('./shared/components/forgot-id/forgot-id.module').then(
                        mod => mod.ForgotIdModule
                    )
            },
            {
                path: 'account-error',
                loadChildren: () =>
                    import('./shared/components/account-error/account-error.module').then(
                        mod => mod.AccountErrorModule
                    )
            },
            {
                path: 'first-login',
                loadChildren: () =>
                    import('./shared/components/first-login/first-login.module').then(
                        mod => mod.FirstLoginModule
                    )
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
