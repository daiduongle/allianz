import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AuthLayoutComponent } from './shared/components/auth-layout/auth-layout.component';

@NgModule({
    declarations: [AppComponent, AuthLayoutComponent],
    imports: [BrowserModule, AppRoutingModule, RouterModule],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
