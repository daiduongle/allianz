import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-auth-layout',
    templateUrl: './auth-layout.component.html',
    styleUrls: ['./auth-layout.component.scss']
})
export class AuthLayoutComponent implements OnInit {
    currentComponent;
    boxTitle = '';

    // Introduction content
    isEnableLoginInstroduction = false;
    isEnableRegisterInstroduction = false;
    isEnableLoginDetails = false;
    isEnableLoginDetails2 = false;
    isEnableForgotPassword1 = false;
    isEnableForgotPassword2 = false;
    isEnableForgotID1 = false;
    isEnableAccountError = false;
    isEnableFirstLogin1 = false;
    isEnableFirstLogin2 = false;

    constructor(private router: Router, private route: ActivatedRoute) {}

    public ngOnInit() {
        this.getChildComponent();
        this.setIntrodutionCondition();
        this.setFormBoxTitle();

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.getChildComponent();
                this.setIntrodutionCondition();
                this.setFormBoxTitle();
            }
        });
    }

    private getChildComponent() {
        const firstChild = this.route.snapshot.firstChild;
        this.currentComponent = firstChild['_routerState'].url;
    }

    setFormBoxTitle() {
        // console.log(this.router.url);
        if (this.router.url.indexOf('forgot-password') >= 0) {
            this.boxTitle = 'Password Reset';
            return;
        }
        if (this.router.url.indexOf('forgot-id') >= 0) {
            this.boxTitle = 'Username Retrieval';
            return;
        }
        if (this.router.url.indexOf('account-error') >= 0) {
            this.boxTitle = 'Username Retrieval';
            return;
        }
        
        this.boxTitle = 'Agent Portal';
        console.log(this.boxTitle);
    }

    setIntrodutionCondition() {
        this.isEnableLoginInstroduction = false;
        this.isEnableRegisterInstroduction = false;
        this.isEnableLoginDetails = false;
        this.isEnableLoginDetails2 = false;
        this.isEnableForgotPassword1 = false;
        this.isEnableForgotPassword2 = false;
        this.isEnableForgotID1 = false;
        this.isEnableAccountError = false;
        this.isEnableFirstLogin1 = false;
        this.isEnableFirstLogin2 = false;

        switch (this.currentComponent) {
            case '/login':
                this.isEnableLoginInstroduction = true;
                break;

            case '/login/1':
                this.isEnableLoginInstroduction = true;
                break;

            case '/login/2':
                this.isEnableLoginInstroduction = true;
                break;

            case '/login/3':
                break;

            case '/login/reset-session':
                this.isEnableLoginInstroduction = true;
                break;

            case '/register':
                this.isEnableRegisterInstroduction = true;
                break;

            case '/register/2':
                this.isEnableLoginDetails = true;
                break;

            case '/register/policy-agent':
                this.isEnableLoginDetails2 = true;
                break;

            case '/forgot-password/1':
                this.isEnableForgotPassword1 = true;
                break;

            case '/forgot-password/2':
                this.isEnableForgotPassword2 = true;
                break;

            case '/forgot-id/1':
                this.isEnableForgotID1 = true;
                break;

            case '/forgot-id/2':
                this.isEnableForgotID1 = true;
                break;
            case '/account-error':
                this.isEnableAccountError = true;
                break;
            case '/first-login/1':
                this.isEnableFirstLogin1 = true;
                break;
            case '/first-login/2':
                this.isEnableFirstLogin2 = true;
                break;
        }
    }
}
