import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {
    updateProfileForm: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.updateProfileForm = this.formBuilder.group({
            mobilePrefix: ['', Validators.required],
            mobile2: ['', Validators.required],
            mobile3: ['', Validators.required],
            secretQuestion: ['', Validators.required],
            secretAnswer: ['', Validators.required]
        });
    }
    onSubmit() {
        this.submitted = true;

        if (this.updateProfileForm.invalid) {
            return;
        }
        this.router.navigate(['/forgot-id/2']);
    }
}
