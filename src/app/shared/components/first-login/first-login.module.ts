import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstLoginComponent } from './first-login.component';
import { FirstLoginRoutes } from './first-login.routing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';

@NgModule({
    imports: [CommonModule, FirstLoginRoutes, ReactiveFormsModule, FormsModule],
    declarations: [FirstLoginComponent,Step1Component, Step2Component]
})
export class FirstLoginModule {}
