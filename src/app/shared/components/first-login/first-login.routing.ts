import { Routes, RouterModule } from '@angular/router';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';

const routes: Routes = [
    { path: '', redirectTo: '1', pathMatch: 'full' },
    { path: '1', component: Step1Component },
    { path: '2', component: Step2Component }
];

export const FirstLoginRoutes = RouterModule.forChild(routes);
