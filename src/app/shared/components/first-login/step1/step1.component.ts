import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-step1',
    templateUrl: './step1.component.html',
    styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
    isHidePassword = true;
    isHidePasswordConfirm = true;
    submitted = false;

    newPasswordForm: FormGroup;

    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.newPasswordForm = this.formBuilder.group({
            password: ['', Validators.required],
            passwordConfirm: ['', Validators.required],
            
        });
    }
    togglePassword() {
        if (this.isHidePassword) {
            this.isHidePassword = false;
            return;
        }
        if (!this.isHidePassword) {
            this.isHidePassword = true;
            return;
        }
    }
    togglePasswordConfirm() {
        if (this.isHidePasswordConfirm) {
            this.isHidePasswordConfirm = false;
            return;
        }
        if (!this.isHidePasswordConfirm) {
            this.isHidePasswordConfirm = true;
            return;
        }
    }
    onSubmit() {
        this.submitted = true;
        
        if (this.newPasswordForm.invalid) {
            return;
        }
        this.router.navigate(['/login/1']);
    }
}
