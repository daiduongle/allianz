import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordComponent } from './forgot-password.component';
import { ForgotPasswordRoutes } from './forgot-password.routing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, ForgotPasswordRoutes],
    declarations: [
        ForgotPasswordComponent,
        Step1Component,
        Step2Component,
        Step3Component
    ]
})
export class ForgotPasswordModule {}
