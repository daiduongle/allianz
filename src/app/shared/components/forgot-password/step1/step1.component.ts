import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-step1',
    templateUrl: './step1.component.html',
    styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
    FPstep1Form: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.FPstep1Form = this.formBuilder.group({
            username: ['', Validators.required],
        });
    }
    onSubmit() {
        this.submitted = true;
        if (this.FPstep1Form.invalid) {
            return;
        }
        this.router.navigate(['/forgot-password/2']);
    }
}
