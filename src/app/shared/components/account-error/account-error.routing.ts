import { Routes, RouterModule } from '@angular/router';
import { AccountErrorComponent } from './account-error.component';

const routes: Routes = [
    { path: '', component: AccountErrorComponent }
];

export const AccountErrorRoutes = RouterModule.forChild(routes);
