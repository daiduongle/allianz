import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountErrorComponent } from './account-error.component';
import { AccountErrorRoutes } from './account-error.routing';

@NgModule({
    imports: [CommonModule, AccountErrorRoutes],
    declarations: [AccountErrorComponent]
})
export class AccountErrorModule {}
