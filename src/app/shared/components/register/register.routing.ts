import { Routes, RouterModule } from '@angular/router';
import { MainRegisterComponent } from './main-register/main-register.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { PolicyAgent2Component } from './policy-agent2/policy-agent2.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', component: MainRegisterComponent },
  { path: '2', component: Step2Component },
  { path: '3', component: Step3Component },
  { path: 'policy-agent', component: PolicyAgent2Component }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutes {}
