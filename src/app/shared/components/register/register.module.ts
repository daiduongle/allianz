import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRegisterComponent } from './main-register/main-register.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { PolicyAgent2Component } from './policy-agent2/policy-agent2.component';
import { RegisterRoutes } from './register.routing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
@NgModule({
    imports: [CommonModule, RegisterRoutes, FormsModule, ReactiveFormsModule],
    declarations: [MainRegisterComponent, Step2Component, Step3Component, PolicyAgent2Component]
})
export class RegisterModule {}
