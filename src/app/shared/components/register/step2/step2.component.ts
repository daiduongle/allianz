import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-step2',
    templateUrl: './step2.component.html',
    styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {
    detailsForm: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.detailsForm = this.formBuilder.group({
            username: ['', Validators.required],
            email: ['', Validators.required]
        });
    }
    onSubmit() {
        this.submitted = true;

        if (this.detailsForm.invalid) {
            return;
        }
        this.router.navigate(['/register/policy-agent']);
    }
}
