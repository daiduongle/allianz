import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-policy-agent2',
    templateUrl: './policy-agent2.component.html',
    styleUrls: ['./policy-agent2.component.scss']
})
export class PolicyAgent2Component implements OnInit {
    policyAgent2Form: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.policyAgent2Form = this.formBuilder.group({
            username: ['', Validators.required]
        });
    }
    onSubmit() {
        this.submitted = true;

        if (this.policyAgent2Form.invalid) {
            return;
        }
        this.router.navigate(['/register/3']);
    }
}
