/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PolicyAgent2Component } from './policy-agent2.component';

describe('PolicyAgent2Component', () => {
  let component: PolicyAgent2Component;
  let fixture: ComponentFixture<PolicyAgent2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyAgent2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyAgent2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
