import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-main-register',
    templateUrl: './main-register.component.html',
    styleUrls: ['./main-register.component.scss']
})
export class MainRegisterComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            agentCode: ['', Validators.required],
            day: ['', Validators.required],
            month: ['', Validators.required],
            year: ['', Validators.required]
        });
    }
    onSubmit() {
        this.submitted = true;

        if (this.registerForm.invalid) {
            return;
        }
        this.router.navigate(['/register/2']);
    }
}
