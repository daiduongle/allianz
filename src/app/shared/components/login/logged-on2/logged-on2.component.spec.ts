/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LoggedOn2Component } from './logged-on2.component';

describe('LoggedOn2Component', () => {
  let component: LoggedOn2Component;
  let fixture: ComponentFixture<LoggedOn2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoggedOn2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedOn2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
