import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-logged-on2',
    templateUrl: './logged-on2.component.html',
    styleUrls: ['./logged-on2.component.scss']
})
export class LoggedOn2Component implements OnInit {
    secretQAForm: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.secretQAForm = this.formBuilder.group({
            secretQuestion: ['', Validators.required],
            secretAnswer: ['', Validators.required]
        });
    }
    onSubmit() {
        this.submitted = true;
        if (this.secretQAForm.invalid) {
            return;
        }
        this.router.navigate(['/login/3']);
    }
}
