import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-logged-on1',
    templateUrl: './logged-on1.component.html',
    styleUrls: ['./logged-on1.component.scss']
})
export class LoggedOn1Component implements OnInit {
    loginForm1: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.loginForm1 = this.formBuilder.group({
            username: ['', Validators.required],
        });
    }

    onSubmit() {
        this.submitted = true;

        if (this.loginForm1.invalid) {
            return;
        }
        this.router.navigate(['/login/reset-session']);
    }
}
