/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LoggedOn1Component } from './logged-on1.component';

describe('LoggedOn1Component', () => {
  let component: LoggedOn1Component;
  let fixture: ComponentFixture<LoggedOn1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoggedOn1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedOn1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
