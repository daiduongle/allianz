import { Routes, RouterModule } from '@angular/router';
import { MainLoginComponent } from './main-login/main-login.component';
import { LoggedOn1Component } from './logged-on1/logged-on1.component';
import { LoggedOn2Component } from './logged-on2/logged-on2.component';
import { LoggedOn3Component } from './logged-on3/logged-on3.component';
import { ResetSessionComponent } from './reset-session/reset-session.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
    { path: '', component: MainLoginComponent },
    { path: '1', component: LoggedOn1Component },
    { path: '2', component: LoggedOn2Component },
    { path: '3', component: LoggedOn3Component },
    { path: 'reset-session', component: ResetSessionComponent }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoginRoutes {}
