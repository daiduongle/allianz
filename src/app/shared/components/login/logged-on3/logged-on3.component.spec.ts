/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LoggedOn3Component } from './logged-on3.component';

describe('LoggedOn3Component', () => {
  let component: LoggedOn3Component;
  let fixture: ComponentFixture<LoggedOn3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoggedOn3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedOn3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
