import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
    selector: 'app-main-login',
    templateUrl: './main-login.component.html',
    styleUrls: ['./main-login.component.scss']
})
export class MainLoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;
    isHidePassword = true;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            termCheck: [false, Validators.required]
            
        });
    }

    togglePassword() {
        if (this.isHidePassword) {
            this.isHidePassword = false;
            return;
        }
        if (!this.isHidePassword) {
            this.isHidePassword = true;
            return;
        }
    }
    onSubmit() {
        this.submitted = true;
        
        if (this.loginForm.invalid || !this.loginForm.value.termCheck) {
            return;
        }
        this.router.navigate(['/login/1']);
    }
}
