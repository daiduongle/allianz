import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-session',
  templateUrl: './reset-session.component.html',
  styleUrls: ['./reset-session.component.scss']
})
export class ResetSessionComponent implements OnInit {
    resetSSForm: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.resetSSForm = this.formBuilder.group({
        activationCode: ['', Validators.required]
    });
  }
  onSubmit() {
    this.submitted = true;
    
    if (this.resetSSForm.invalid) {
        return;
    }
    this.router.navigate(['/login/2']);
}
}
