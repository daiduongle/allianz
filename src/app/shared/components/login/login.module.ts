import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLoginComponent } from './main-login/main-login.component';
import { LoggedOn1Component } from './logged-on1/logged-on1.component';
import { LoggedOn2Component } from './logged-on2/logged-on2.component';
import { LoggedOn3Component } from './logged-on3/logged-on3.component';
import { ResetSessionComponent } from './reset-session/reset-session.component';
import { LoginRoutes } from './login.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, LoginRoutes, FormsModule, ReactiveFormsModule],
    providers: [],
    declarations: [
        ResetSessionComponent,
        MainLoginComponent,
        LoggedOn1Component,
        LoggedOn2Component,
        LoggedOn3Component
    ]
})
export class LoginModule {}
