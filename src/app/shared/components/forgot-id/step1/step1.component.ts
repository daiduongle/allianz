import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
    forgotID1Form: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private router: Router) {}

    ngOnInit() {
        this.forgotID1Form = this.formBuilder.group({
            agentCode: ['', Validators.required],
            agencyCode: ['', Validators.required],
            day: ['', Validators.required],
            month: ['', Validators.required],
            year: ['', Validators.required]
        });
    }
    onSubmit() {
        this.submitted = true;

        if (this.forgotID1Form.invalid) {
            return;
        }
        this.router.navigate(['/forgot-id/2']);
    }
}
