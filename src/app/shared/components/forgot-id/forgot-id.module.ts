import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotIdComponent } from './forgot-id.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { ForgotIdRoutes } from './forgot-id.routing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, ForgotIdRoutes],
    declarations: [ForgotIdComponent, Step1Component, Step2Component, Step3Component]
})
export class ForgotIdModule {}
