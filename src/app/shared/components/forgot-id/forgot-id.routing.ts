import { Routes, RouterModule } from '@angular/router';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';

const routes: Routes = [
    { path: '', redirectTo: '1', pathMatch: 'full' },
    { path: '1', component: Step1Component },
    { path: '2', component: Step2Component },
    { path: '3', component: Step3Component }
];

export const ForgotIdRoutes = RouterModule.forChild(routes);
